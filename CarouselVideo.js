import React, { useState } from "react";
import { LeftCircleOutlined, RightCircleOutlined } from "@ant-design/icons";
import { Row, Col } from "antd";
const jsondata = [
  {
    id: 1,
    quality: "hd",
    file_type: "video/mp4",
    width: 100,
    height: 1920,
    link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
  },
  {
    id: 2,
    quality: "hd",
    file_type: "video/mp4",
    width: 1080,
    height: 1920,
    link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
  },
  {
    id: 3,
    quality: "hd",
    file_type: "video/mp4",
    width: 1080,
    height: 1920,
    link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
  },
  {
    id: 4,
    quality: "hd",
    file_type: "video/mp4",
    width: 100,
    height: 1920,
    link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
  },
  {
    id: 5,
    quality: "hd",
    file_type: "video/mp4",
    width: 1080,
    height: 1920,
    link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
  },
  {
    id: 6,
    quality: "hd",
    file_type: "video/mp4",
    width: 1080,
    height: 1920,
    link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
  },
  {
    id: 7,
    quality: "hd",
    file_type: "video/mp4",
    width: 100,
    height: 1920,
    link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
  },
  {
    id: 8,
    quality: "hd",
    file_type: "video/mp4",
    width: 1080,
    height: 1920,
    link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
  },
  {
    id: 9,
    quality: "hd",
    file_type: "video/mp4",
    width: 1080,
    height: 1920,
    link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
  },
];
const length = 9;
let start = 3;

function CarouselVideo() {
  const [json, setJson] = useState([
    {
      id: 1,
      quality: "hd",
      file_type: "video/mp4",
      width: 100,
      height: 1920,
      link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
    },
    {
      id: 2,
      quality: "hd",
      file_type: "video/mp4",
      width: 1080,
      height: 1920,
      link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
    },
    {
      id: 3,
      quality: "hd",
      file_type: "video/mp4",
      width: 1080,
      height: 1920,
      link: "https://player.vimeo.com/external/342571552.hd.mp4?s=6aa6f164de3812abadff3dde86d19f7a074a8a66&profile_id=175&oauth2_token_id=57447761",
    },
  ]);
  const moveright = () => {
    setJson(jsondata.slice(start, start + 3));
    start = start + 3;
  };
  const moveleft = () => {
    start = start - 3;
    setJson(jsondata.slice(start - 3, start));
  };

  return (
    // <Carousel useKeyboardArrows>
    //   <div>
    //     <Videos />
    //   </div>
    //   <div>
    //     <Videos />
    //   </div>
    //   <div>
    //     <Videos />
    //   </div>
    // </Carousel>
    <>
      <Row align="middle">
        <Col span={2}>
          {start > 3 && (
            <LeftCircleOutlined
              style={{ fontSize: "32px" }}
              onClick={moveleft}
              disabled={start < 0}
            />
          )}
        </Col>
        <Col span={20}>
          <Row>
            {json.map((data) => (
              <>
                <Col span={8}>
                  <video src={data.link} id="video" />
                  {data.id}
                </Col>
              </>
            ))}
          </Row>
        </Col>
        <Col span={2} align="middle">
          {start < 9 && (
            <RightCircleOutlined
              style={{ fontSize: "32px" }}
              onClick={moveright}
              disabled={length - start < 3}
            />
          )}
        </Col>
      </Row>
    </>
  );
}

export default CarouselVideo;
